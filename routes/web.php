<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\DatabaseController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\RolesController;
use App\Http\Controllers\TypesController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('home');
});*/

Route::get('database-example', [DatabaseController::class, 'index'])->name('database-example');

Route::redirect('/', '/admin');

Route::get('/', [HomeController::class, 'showHome'])->name('home');
Route::get('/login', [LoginController::class, 'showForm'])->name('login');
Route::post('/login', [LoginController::class, 'login']);
Route::post('/logout', [LoginController::class, 'logout'])->name('logout');

Route::prefix('/blog')->name('blog')->group(function ()
{
    Route::get('/', [PostController::class, 'index'])->name('index_blog');
});

Route::middleware(['authentication'])->group(function ()
{
    Route::get('/admin', [AdminController::class, 'showAdmin'])->name('admin');
    Route::prefix('/users')->group(function ()
    {
        Route::get('/', [UsersController::class, 'index'])->name('index_users');
        Route::get('/create', [UsersController::class, 'create'])->name('create_users');
        Route::get('/edit/{id}', [UsersController::class, 'edit'])->name('edit_users');
        Route::match(['POST', 'PUT', 'PATCH'], '/{id?}', [UsersController::class, 'save'])->name('save_users');
        Route::delete('/{id}', [UsersController::class, 'delete'])->name('delete_users');
    });

    Route::prefix('/roles')->group(function ()
    {
        Route::get('/', [RolesController::class, 'index'])->name('index_roles');
        Route::any('/get', [RolesController::class, 'get'])->name('get_roles');
        Route::get('/create', [RolesController::class, 'create'])->name('create_roles');
        Route::get('/edit/{id}', [RolesController::class, 'edit'])->name('edit_roles');
        Route::match(['POST', 'PUT', 'PATCH'], '{id?}', [RolesController::class, 'save'])->name('save_roles');
        Route::delete('/{id}', [RolesController::class, 'delete'])->name('delete_roles');
    });

    Route::prefix('/types')->group(function ()
    {
        Route::get('/', [TypesController::class, 'index'])->name('index_types');
        Route::get('/create', [TypesController::class, 'create'])->name('create_types');
        Route::get('/edit/{id}', [TypesController::class, 'edit'])->name('edit_types');
        Route::match(['POST', 'PUT', 'PATCH'], '{id?}', [TypesController::class, 'save'])->name('save_types');
        Route::delete('/{id}', [TypesController::class, 'delete'])->name('delete_types');
    });
});
