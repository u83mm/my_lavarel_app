<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="title" content="Web site" /> 
		<meta name="description" content="Proyectos basados en el framework php Laravel" />
		<meta name="keywords" content="diseño web, desarrollo web, framework symfony, php, programación web" />
		<meta name="robots" content="All" />  
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <title>@section('title')Title @show('title')</title>   
        {{-- <link rel="icon" href="data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 128 128%22><text y=%221.2em%22 font-size=%2296%22>⚫️</text></svg>"> --}}
        {{-- Run `composer require symfony/webpack-encore-bundle` to start using Symfony UX --}}
        @section('stylesheets')
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <link rel="stylesheet" type="text/css" href={{ asset('css/estilo.css') }}>
            <link href={{ asset('css/all.css') }} rel="stylesheet">    
        @section('stylesheets')                         	       
        @section('javascripts')
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
            <script type="text/javascript" src={{ asset('js/eventos.js') }}></script> 
            <script defer src="{{ asset('js/all.js') }}"></script> 
            {{-- <script src={{ asset('https://kit.fontawesome.com/80ec316a17.js') }} crossorigin="anonymous"></script> --}}
        @section('javascripts')       	       
    </head>
    <body>
    	<div class="container-fluid">
			<header class="text-center shadow rounded bg-success p-2 bg-opacity-25 border border-primary ">
				<div class="container-fluid">
					<div class="row align-items-center">						
						<div class="col-12 col-md-10 col-lg-12">							
							<h1 class="text-success">Proyecto con Laravel</h1>                           
						</div>						
					</div>										
				</div>				
			</header>			                            
            <nav class="navbar navbar-light bg-light sticky-top">                				
				<div class="container-fluid">
                    @section('nav')					
					<div class="col-3 col-sm-1 col-lg-1">
						<a class="navbar-brand" href="#"><img src="#" class="img-fluid float-start" alt="imagen_logo"></a>
					</div>
					<ul class="nav nav-pills">
						<li class="nav-item d-none d-sm-none d-lg-inline-block">
							<a class="nav-link" aria-current="page" href="{{ route('home') }}">Home</a>
						</li>
						<li class="nav-item d-none d-sm-none d-lg-inline-block">
                            @if (!$session)
                                <a class="nav-link" aria-current="page" href="{{ route('login') }}">Login</a>
                            @else
                                <a class="nav-link" aria-current="page" href="javascript:void(0)" onclick="$('logout').submit();">Logout</a>
                                <form id="logout" action="{{ route('logout') }}" method="POST">@csrf</form>             
                            @endif							
						</li>						
					</ul>
					<button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasNavbar" aria-controls="offcanvasNavbar">
					  <span class="navbar-toggler-icon"></span>
					</button>
					<div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasNavbar" aria-labelledby="offcanvasNavbarLabel">
						<div class="offcanvas-header">
							<h5 class="offcanvas-title" id="offcanvasNavbarLabel">Link</h5>
							<button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
						</div>
						<div class="offcanvas-body">
							<ul class="navbar-nav justify-content-end flex-grow-1 pe-3">
								<li class="nav-item">
									<a class="nav-link active" aria-current="page" href="{{ route('home') }}"><i class="fas fa-home fa-lg fa-fw"></i> Home</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="#"><i class="fas fa-video fa-lg fa-fw"></i> Link</a>
								</li>								
								<li class="nav-item">
									<a class="nav-link" href="#"><i class="fas fa-envelope fa-lg fa-fw"></i> Link</a>
								</li>
								<li class="nav-item dropdown">
									<a class="nav-link dropdown-toggle" href="#" id="offcanvasNavbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Acceso</a>
									<ul class="dropdown-menu" aria-labelledby="offcanvasNavbarDropdown">
										<li>
											<a class="dropdown-item" href="{{ route('login') }}">Login</a>
										</li>																		
									</ul>
							  	</li>
							</ul>						
						</div>
					</div>
                    @show
				</div>                			
			</nav>              										
			<main class="pt-3 pb-3">
				@section('flash')                                    
                    {{--read and display all flash messages --}}                    
                    {{-- {% for label, messages in app.flashes %}
                        {% for message in messages %}
                            <div class="flash-{{ label }}">
                                {{ message }}
                            </div>
                        {% endfor %}
                    {% endfor %} --}}                   					
                @show('flash')									
				@section('main')@show
			</main>
			<footer class="text-center rounded bg-success p-2 bg-opacity-25 border border-primary">
				@section('footer')
                <div class="container-fluid p-xl-5">						
                    <div class="row">
                        <div class="col-sm-6 col-lg-3 pe-xl-5 ps-xl-5 mb-4">
                            <h4>Proyectos</h4>
                            <p>Accede a la documentación en línea de los proyectos disponibles.</p>
                            <a href="#">Link</a>								
                        </div>
                        <div class="col-sm-6 col-lg-3 pe-xl-5 ps-xl-5 mb-4">
                            <h4>Redes Sociales</h4>
                            <p>Accede a mis redes sociales donde podrás encontrar información sobre mí y sobre el trabajo que desarrollo, 
                            así como a diverso contenido.</p>
                            <div class="row">
                                <div class="col-2">
                                    <a href="https://www.youtube.com/channel/UCKl7p8vA-o-_V6ZqcItrs2Q" target="_blank" title="Visita mi canal" target="_blank"><span class="youtubeIcon"><i class="fab fa-youtube fa-2x fa-fw"></i></span></a>&nbsp;	
                                </div>
                                <div class="col-2">
                                    <a href="https://www.linkedin.com/in/jos%C3%A9-mario-m-b0236037/" target="_blank" title="Aquí me encontrarás"><span class="linkedinIcon"><i class="fab fa-linkedin fa-2x fa-fw"></i></span></a>&nbsp;
                                </div>								
                            </div>
                            <p>Si te gusta el contenido y quieres contribuir a que pueda seguir subiendo contenido a mi canal, puedes hacer una 
                            donación a través del botón donar o mediante el códig QR. <br>Muchas gracias!!.</p>
                            <div class="row text-center">
                                <div class="col-6">
                                    <form action="https://www.paypal.com/donate" method="post" target="_top">
                                        <input type="hidden" name="hosted_button_id" value="LYTLPW2LM6DWC" />
                                        <input class="donarPaypal" type="image" src="https://www.paypalobjects.com/es_ES/ES/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Botón Donar con PayPal" />
                                        <img alt="boton-paypal" border="0" src="https://www.paypal.com/es_ES/i/scr/pixel.gif" width="1" height="1" />
                                    </form>
                                </div>
                                <div class="col-6">
                                    <figure class="figure w-75">
                                        <img src="{{ asset('images/QR.png') }}" class="figure-img img-fluid float-none float-sm-start w-100" alt="imagen">
                                    </figure>
                                </div>																									
                            </div>															
                        </div>
                        <div class="col-sm-6 col-lg-3 pe-xl-5 ps-xl-5 mb-4">
                            <h4>Sobre mí</h4>
                            <p>Apasionado del desarrollo de aplicaciones con tecnologías web. Diseño aplicaciones web personalizadas
                            adaptadas a las necesidades del cliente. Comprometido con la eficacia y la responsabilidad. Para cada objetivo, 
                            un proyecto. 📈️</p>
                        </div>
                        <div class="col-sm-6 col-lg-3 pe-xl-5 ps-xl-5 mb-4">
                            <h4>Contacto</h4>								
                            <p>Puedes contactarme a través de correo electrónico, por teléfono ó por whatssap. También puedes acceder
                            a la sección <a href="#">Contacto</a> dónde podrás enviarme tus datos y un comentario
                            si deseas que contacte contigo para alguna idea que quieras desarrollar para tu negocio.</p>
                            <div class="col-12 text-start">
                                <a href="mailto:cursotecnoweb@gmail.com" title="Envíame un correo"><span class="mailIcon"><i class="fas fa-envelope fa-lg fa-fw"></i></span> Escríbeme un Email</a>
                            </div>
                            <div class="col-12 text-start">
                                <a href="tel:+34660176387" title="Llámame"><span class="phoneIcon"><i class="fas fa-mobile-alt fa-lg fa-fw"></i></span> +34 660176387</a>
                            </div>
                            <div class="col-12 text-start">
                                <span class="whatsappIcon d-inline" title="Contacta a través de whatsapp"><i class="fab fa-whatsapp-square fa-lg fa-fw"></i></span> +34 660176387
                            </div>
                        </div>
                        <div class="row pt-5">
                            <p class="text-center">Copyright &copy;2022 - {{ config('globals.year') }} Made by <em>José Mario Moreno Montenegro</em></p>
                        </div>
                    </div>
                </div>
               	@show('footer')												
			</footer> 
		</div>       
    </body>
</html>
