<form class="col-3 m-auto" action="#" method="POST">
    @csrf
    <div class="row mb-3">
        <label class="col-sm-3 col-form-label" for="email">Email:</label>
        <div class="col-sm-9">
            <input class="form-control" type="email" name="email" id="mail">
        </div>
    </div>
    <div class="row mb-3">
        <label class="col-sm-3 col-form-label" for="password">Password:</label>
        <div class="col-sm-9">
            <input class="form-control" type="password" name="password" id="password">
        </div>
    </div>
    <div class="row mb-3">
        <label class="col-sm-3 col-form-label" for="password">&nbsp;</label>
        <div class="col-sm-9">
            <button class="btn btn-primary">Enviar</button>
        </div>
    </div>                                    
</form>