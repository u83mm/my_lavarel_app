@extends('base')
@section('main')
    @isset($message)
        <p class="text-center {{ $alert }}">{{ $message }}</p>        
    @endisset
    <h3>User Roles</h3>

    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Date Adding</th>
                    <th>Date Updating</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>           
                @foreach ($roles as $r)
                <tr>
                    <td>{{ $r['id'] }}</td>
                    <td>{{ $r['label'] }}</td>
                    <td>{{ $r['created_at'] }}</td>
                    <td>{{ $r['updated_at'] }}</td>
                    <td>
                        <a class="btn btn-success" href="{{ route('edit_roles', ['id' => $r['id']]) }}">Edit</a>
                        <form class="d-inline-block" action="{{ route('delete_roles', ['id' => $r['id']]) }}" method="POST" onsubmit="return confirm('¿Estás seguro de querer eliminar el registro?')">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
                @endforeach            
            </tbody>
        </table>
    </div>
    
    <a class="btn btn-primary" href="{{ route('create_roles') }}">New Role</a>    
@endsection
