@extends('base')
@section('main')
    @isset($message)
        <p class="text-center {{ $alert }}">{{ $message }}</p>        
    @endisset
    <h3 class="text-center">
        User Roles / 
        @if ($id == null)
            New Role
        @else
            Edit Role
        @endif
    </h3>
    <div class="row justify-content-center">
        <div class="col-12 col-md-4">
            <form action="{{ route('save_roles', ['id' => $id]) }}" method="POST">
                @csrf
                @if ($id != null)
                    @method('PUT')
                    <input type="hidden" name="id" value="{{ $id }}">
                @endif
                <label for="name">Role</label>
                <input type="text" name="name" id="name" value="{{ $record != null ? $record['label'] : "" }}">
                <button class="btn btn-success">Save</button>
            </form>
            <a class="btn btn-primary" href="{{ route('index_roles') }}">Back</a>
        </div>               
    </div>
    
@endsection
