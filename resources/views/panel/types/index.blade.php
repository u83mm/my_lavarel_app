@extends('base')
@section('main')
    @isset($message)
        <p class="text-center {{ $alert }}">{{ $message }}</p>        
    @endisset
    <h3>User Types</h3>

    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Date Adding</th>
                    <th>Date Updating</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>           
                @foreach ($types as $t)
                <tr>
                    <td>{{ $t['id'] }}</td>
                    <td>{{ $t['label'] }}</td>
                    <td>{{ $t['created_at'] }}</td>
                    <td>{{ $t['updated_at'] }}</td>
                    <td>
                        <a class="btn btn-success" href="{{ route('edit_types', ['id' => $t['id']]) }}">Edit</a>
                        <form class="d-inline-block" action="{{ route('delete_types', ['id' => $t['id']]) }}" method="POST" onsubmit="return confirm('¿Estás seguro de querer eliminar el registro?')">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
                @endforeach            
            </tbody>
        </table>
    </div>
    
    <a class="btn btn-primary" href="{{ route('create_types') }}">New Type</a>    
@endsection
