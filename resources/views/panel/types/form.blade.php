@extends('base')
@section('main')
    @isset($message)
        <p class="text-center {{ $alert }}">{{ $message }}</p>        
    @endisset
    <h3 class="text-center">
        User types / 
        @if ($id == null)
            New Type
        @else
            Edit Type
        @endif
    </h3>

    <div class="row justify-content-center">
        <div class="col-12 col-md-4">
            <form action="{{ route('save_types', ['id' => $id]) }}" method="POST">
                @csrf
                @if ($id != null)
                    @method('PUT')
                    <input type="hidden" name="id" value="{{ $id }}">
                @endif
                <label for="name">Type</label>
                <input type="text" name="name" id="name" value="{{ $record != null ? $record['label'] : "" }}">
                <button class="btn btn-primary">Save</button>
            </form>
            <a class="btn btn-primary" href="{{ route('index_types') }}">Back</a>
        </div>
    </div>
    
@endsection
