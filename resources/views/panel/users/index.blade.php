@extends('base')
@section('main')
    @isset($message)
        <p class="text-center {{ $alert }}">{{ $message }}</p>        
    @endisset
    <h3>User Index</h3>

    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Date Adding</th>
                    <th>Date Updating</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>           
                @foreach ($users as $u)
                <tr>
                    <td>{{ $u['id'] }}</td>
                    <td>{{ $u['name'] }}</td>
                    <td>{{ $u['email'] }}</td>
                    <td>{{ $u['created_at'] }}</td>
                    <td>{{ $u['updated_at'] }}</td>
                    <td>
                        <a class="btn btn-success" href="{{ route('edit_users', ['id' => $u['id']]) }}">Edit</a>
                        <form class="d-inline-block" action="{{ route('delete_users', ['id' => $u['id']]) }}" method="POST" onsubmit="return confirm('¿Estás seguro de querer eliminar el registro?')">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
                @endforeach            
            </tbody>
        </table>
    </div>
    
    <a class="btn btn-primary" href="{{ route('create_users') }}">New User</a>    
@endsection