@extends('base')
@section('main')
    @isset($message)
        <p class="text-center {{ $alert }}">{{ $message }}</p>        
    @endisset
    <h3 class="text-center">
        User / 
        @if ($id == null)
            New User
        @else
            Edit User
        @endif
    </h3>

    <div class="row justify-content-center">
        <div class="col-12 col-md-4">
            <form action="{{ route('save_users', ['id' => $id]) }}" method="POST">
                @csrf
                @if ($id != null)
                    @method('PUT')
                    <input type="hidden" name="id" value="{{ $id }}">
                @endif
                <div class="row mb-3">
                    <label class="col-form-label" for="name">User</label>
                    <input class="form-control" type="text" name="name" id="name" value="{{ $record != null ? $record['name'] : "" }}">
                </div>
                <div class="row mb-3">
                    <label class="col-form-label" for="email">Email</label>
                    <input class="form-control" type="email" name="email" id="email" value="{{ $record != null ? $record['email'] : "" }}">
                </div>
               
                <div class="row col-2 d-inline-block">
                    <button class="btn btn-success">Save</button>
                </div>  
                <div class="row col-2 d-inline-block ms-3">
                    <a class="btn btn-primary" href="{{ route('index_users') }}">Back</a>
                </div>               
            </form>                        
        </div>
    </div>        
@endsection