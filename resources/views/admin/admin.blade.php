@extends('base')
@section('main')
    @isset($message)
        <p class="text-center {{ $alert }}">{{ $message }}</p>        
    @endisset
    
    <h3>Página Admin</h3>
    <ul>
        <li>
            <a href="{{ route('index_roles') }}">Roles</a>
        </li>
        <li>
            <a href="{{ route('index_types') }}">Types</a>
        </li>
        <li>
            <a href="{{ route('index_users') }}">Users</a>
        </li>
    </ul>
    
@endsection