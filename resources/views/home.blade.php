@extends('base')
@section('main')
    @isset($message)
        <p class="text-center {{ $alert }}">{{ $message }}</p>        
    @endisset    
@endsection