<?php

namespace App\Http\Controllers;

use App\Exceptions\MyCustomException;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        try {
            $latestPost = Post::with(['Category', 'author'])->orderBy('created_at', 'desc')->limit(10)->get();
            return view('blog.index', [
               'session' => \request()->session()->get('user'),
               'posts' => $latestPost,               
            ]);                        
        } catch (MyCustomException $e) {
            $msg =  $e->getMessage() . " en la línea " . $e->getLine() . " del archivo " . $e->getFile();
            return view('blog.index', [
                'message' => $msg,
                'alert' => 'alert alert-danger',
            ]);
        } catch (\Throwable $th) {
            $msg = $th->getMessage();
            return view('blog.index', [
                'message' => $msg,
                'alert' => 'alert alert-danger',
            ]);
        }           
    }

    public function save(Request $request)
    {
        # code...
    }

    public function find($uri,)
    {
        # code...
    }

    public function delete($id)
    {
        # code...
    }
}
