<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function showAdmin(Request $request)
    {
        return view('admin.admin', [
            'session' => $request->session()->get('user'),
        ]);
    }
}
