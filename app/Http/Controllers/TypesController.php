<?php

namespace App\Http\Controllers;

use App\Exceptions\MyCustomException;
use Illuminate\Http\Request;

class TypesController extends Controller
{   

    public function index()
    {   
        // Temporary data until manage the Data Base

        $types = [
            [
                'id' => 1,
                'label' => 'Normal',
                'created_at' => '2022-08-03 22:52:00',
                'updated_at' => '2022-08-03 22:52:00'
            ],
            [
                'id' => 2,
                'label' => 'Admin',
                'created_at' => '2022-08-03 22:52:00',
                'updated_at' => '2022-08-03 22:52:00'
            ],
            [
                'id' => 3,
                'label' => 'Super Admin',
                'created_at' => '2022-08-03 22:52:00',
                'updated_at' => '2022-08-03 22:52:00'
            ],
        ];

        try {
            return view('panel.types.index', [
               'session' => \request()->session()->get('user'),
               'types' => $types 
            ]);
            
        } catch (MyCustomException $e) {
            $msg =  $e->getMessage() . " en la línea " . $e->getLine() . " del archivo " . $e->getFile();
            return view('panel.types.index', [
                'message' => $msg,
                'alert' => 'alert alert-danger',
            ]);
        } catch (\Throwable $th) {
            $msg = $th->getMessage();
            return view('panel.types.index', [
                'message' => $msg,
                'alert' => 'alert alert-danger',
            ]);
        }   
    }

    public function create()
    {
        try {
            return view('panel.types.form', [
               'session' => \request()->session()->get('user'),
               'id' => null,
               'record' => null
            ]);
            
        } catch (MyCustomException $e) {
            $msg =  $e->getMessage() . " en la línea " . $e->getLine() . " del archivo " . $e->getFile();
            return view('panel.types.form', [
                'message' => $msg,
                'alert' => 'alert alert-danger',
            ]);
        } catch (\Throwable $th) {
            $msg = $th->getMessage();
            return view('panel.types.form', [
                'message' => $msg,
                'alert' => 'alert alert-danger',
            ]);
        }   
    }

    public function edit($id)
    {
        // Temporary data until manage the Data Base

        $types = [            
            'id' => 1,
            'label' => 'Normal',
            'created_at' => '2022-08-03 22:52:00',
            'updated_at' => '2022-08-03 22:52:00'
        ];                   

        try {
            return view('panel.types.form', [
               'session' => \request()->session()->get('user'),
               'id' => $id,
               'record' => $types
            ]);
            
        } catch (MyCustomException $e) {
            $msg =  $e->getMessage() . " en la línea " . $e->getLine() . " del archivo " . $e->getFile();
            return view('panel.types.form', [
                'message' => $msg,
                'alert' => 'alert alert-danger',
            ]);
        } catch (\Throwable $th) {
            $msg = $th->getMessage();
            return view('panel.types.form', [
                'message' => $msg,
                'alert' => 'alert alert-danger',
            ]);
        }   
    }

    public function save(Request $request, $id = null)
    {
        try {
            // Temporary data until manage the Data Base

            $types = [
                [
                    'id' => 1,
                    'label' => 'Normal',
                    'created_at' => '2022-08-03 22:52:00',
                    'updated_at' => '2022-08-03 22:52:00'
                ],
                [
                    'id' => 2,
                    'label' => 'Admin',
                    'created_at' => '2022-08-03 22:52:00',
                    'updated_at' => '2022-08-03 22:52:00'
                ],
                [
                    'id' => 3,
                    'label' => 'Super Admin',
                    'created_at' => '2022-08-03 22:52:00',
                    'updated_at' => '2022-08-03 22:52:00'
                ],
            ];            

            $options = [
                'session' => \request()->session()->get('user'),
                'types' => $types,
                'message' => "",
                'alert' => 'alert bg-success text-white',
            ];


            // If field Name is empty

            if ($request->input('name') == null) {
                $options['message'] = 'You must enter a name';
                $options['alert'] = 'alert alert-danger';
                $options['id'] = null;
                $options['record'] = null;

                return view('panel.types.form', $options);  
            }

            if($request->isMethod('POST')) {
                 $options['message'] = 'Type added successfully';            
            }

            if($request->isMethod('PUT') || $request->isMethod('PATCH')) {
                $options['message'] = 'Type updated successfully';            
            }

            return view('panel.types.index', $options);            
                        
        } catch (MyCustomException $e) {
            $msg =  $e->getMessage() . " en la línea " . $e->getLine() . " del archivo " . $e->getFile();
            return view('panel.types.index', [
                'message' => $msg,
                'alert' => 'alert alert-danger',
            ]);
        } catch (\Throwable $th) {
            $msg = $th->getMessage();
            return view('panel.types.index', [
                'message' => $msg,
                'alert' => 'alert alert-danger',
            ]);
        }         
    }

    public function delete($id, Request $request)
    {
        try {
            // Temporary data until manage the Data Base

            $types = [
                [
                    'id' => 1,
                    'label' => 'Normal',
                    'created_at' => '2022-08-03 22:52:00',
                    'updated_at' => '2022-08-03 22:52:00'
                ],
                [
                    'id' => 2,
                    'label' => 'Admin',
                    'created_at' => '2022-08-03 22:52:00',
                    'updated_at' => '2022-08-03 22:52:00'
                ],
                [
                    'id' => 3,
                    'label' => 'Super Admin',
                    'created_at' => '2022-08-03 22:52:00',
                    'updated_at' => '2022-08-03 22:52:00'
                ],
            ];            

            $options = [
                'session' => \request()->session()->get('user'),
                'types' => $types,
                'message' => "",
                'alert' => 'alert bg-success text-white',
            ];                      

            if($request->isMethod('DELETE')) {
                $options['message'] = 'Role deleted successfully';            
            }

            return view('panel.types.index', $options);            
                        
        } catch (MyCustomException $e) {
            $msg =  $e->getMessage() . " en la línea " . $e->getLine() . " del archivo " . $e->getFile();
            return view('panel.types.index', [
                'message' => $msg,
                'alert' => 'alert alert-danger',
            ]);
        } catch (\Throwable $th) {
            $msg = $th->getMessage();
            return view('panel.types.index', [
                'message' => $msg,
                'alert' => 'alert alert-danger',
            ]);
        } 
    }
}
