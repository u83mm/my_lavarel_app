<?php

namespace App\Http\Controllers;

use App\Exceptions\MyCustomException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function showForm(Request $request)
    {
        try {
            return view('login', [
                'session' => $request->session()->get('user'),
            ]);
            
        } catch (MyCustomException $e) {
            $msg =  $e->getMessage() . " en la línea " . $e->getLine() . " del archivo " . $e->getFile();
            return view('login', [
                'message' => $msg,
                'alert' => 'alert alert-danger',
                'session' => $request->session()->get('user'),
            ]);
        } catch (\Throwable $th) {
            $msg = $th->getMessage();
            return view('login', [
                'message' => $msg,
                'alert' => 'alert alert-danger',
                'session' => $request->session()->get('user'),
            ]);
        }    
    }

    public function login(Request $request)
    {
        $input = $request->only('email', 'password');       
        $access = $this->checkCredentials($input);

        if(!$access) {
            return view('login', [
                'message' => "Bad credentials.",
                'alert' => 'alert alert-danger',
                'session' => $request->session()->get('user'),
            ]);
        }

        $request->session()->put('user', $input);     

        return redirect()->route('admin', [
            
        ]);
    }

    public function logout(Request $request )
    {
        $request->session()->forget('user');
        return redirect()->route('home');
    }

    private function checkCredentials($input)
    {        
        $credentials = [
            'mail' => $input['email'],
            'password' => $input['password'],
        ];        
        
        /*if ($credentials['mail'] == $input['mail'] && $credentials['password'] == $input['password']) {
            return $credentials;
        }*/
        
        $result = DB::table('users')->select('password')->where('email', $input['email'])->get();
        $user = $result->toArray();        

        if (Hash::check($input['password'], $user[0]->password)) {
            return $credentials;
        }       
    } 
}
