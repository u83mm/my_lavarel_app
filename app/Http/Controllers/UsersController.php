<?php

namespace App\Http\Controllers;

use App\Exceptions\MyCustomException;
use App\Models\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function index()
    {   
        $user = new User();
        $users = $user->get()->toArray();                

        try {
            return view('panel.users.index', [
               'session' => \request()->session()->get('user'),
               'users' => $users 
            ]);
            
        } catch (MyCustomException $e) {
            $msg =  $e->getMessage() . " en la línea " . $e->getLine() . " del archivo " . $e->getFile();
            return view('panel.users.index', [
                'message' => $msg,
                'alert' => 'alert alert-danger',
            ]);
        } catch (\Throwable $th) {
            $msg = $th->getMessage();
            return view('panel.users.index', [
                'message' => $msg,
                'alert' => 'alert alert-danger',
            ]);
        }   
    }

    public function create()
    {
        try {
            return view('panel.users.form', [
               'session' => \request()->session()->get('user'),
               'id' => null,
               'record' => null
            ]);
            
        } catch (MyCustomException $e) {
            $msg =  $e->getMessage() . " en la línea " . $e->getLine() . " del archivo " . $e->getFile();
            return view('panel.users.form', [
                'message' => $msg,
                'alert' => 'alert alert-danger',
            ]);
        } catch (\Throwable $th) {
            $msg = $th->getMessage();
            return view('panel.users.form', [
                'message' => $msg,
                'alert' => 'alert alert-danger',
            ]);
        }   
    }

    public function edit($id)
    {
        // Temporary data until manage the Data Base
        $user = new User();                      

        try {
            return view('panel.users.form', [
               'session' => \request()->session()->get('user'),
               'id' => $id,
               'record' =>  $user->find($id)->toArray()
            ]);
            
        } catch (MyCustomException $e) {
            $msg =  $e->getMessage() . " en la línea " . $e->getLine() . " del archivo " . $e->getFile();
            return view('panel.users.form', [
                'message' => $msg,
                'alert' => 'alert alert-danger',
            ]);
        } catch (\Throwable $th) {
            $msg = $th->getMessage();
            return view('panel.users.form', [
                'message' => $msg,
                'alert' => 'alert alert-danger',
            ]);
        }   
    }

    public function save(Request $request, $id = null)
    {
        try {                                
            $user = new User();            

            $input = $request->except('_token');
            
            if (!isset($input['password'])) {
                $input['password'] = 'dummy';
            }
                        
            $user->updateOrCreate(
                [                
                    'id' => $id
                ], 
                [
                    'name' => $input['name'],
                    'email' => $input['email'],
                    'password' => $input['password'],
                ]
            );
            
            $options = [
                'session' => \request()->session()->get('user'),
                'users' => $user->get()->toArray(),
                'message' => "",
                'alert' => 'alert bg-success text-white',
            ];                          
            
            // If field Name is empty

            if ($request->input('name') == null) {               
                $options['message'] = 'You must enter a name';
                $options['alert'] = 'alert alert-danger';
                $options['id'] = null;
                $options['record'] = null;               

                return view('panel.users.form', $options);  
            }

            if($request->isMethod('POST')) {
                 $options['message'] = 'User added successfully';                            
            }

            if($request->isMethod('PUT') || $request->isMethod('PATCH')) {               
                $options['message'] = 'User updated successfully';                                                       
            }                                     

            return view('panel.users.index', $options);            
                        
        } catch (MyCustomException $e) {
            $msg =  $e->getMessage() . " en la línea " . $e->getLine() . " del archivo " . $e->getFile();
            return view('panel.users.index', [
                'message' => $msg,
                'alert' => 'alert alert-danger',
            ]);
        } catch (\Throwable $th) {
            $msg = $th->getMessage();
            return view('panel.users.index', [
                'message' => $msg,
                'alert' => 'alert alert-danger',
            ]);
        }         
    }

    public function delete($id, Request $request)
    {
        try {
            // Temporary data until manage the Data Base

            $user = new User();               

            $user->deleteUser($id);

            $options = [
                'session' => \request()->session()->get('user'),
                'users' => $user->get()->toArray(),
                'message' => "",
                'alert' => 'alert bg-success text-white',
            ];                      

            if($request->isMethod('DELETE')) {
                $options['message'] = 'Role deleted successfully';            
            }

            return view('panel.users.index', $options);            
                        
        } catch (MyCustomException $e) {
            $msg =  $e->getMessage() . " en la línea " . $e->getLine() . " del archivo " . $e->getFile();
            return view('panel.users.index', [
                'message' => $msg,
                'alert' => 'alert alert-danger',
            ]);
        } catch (\Throwable $th) {
            $msg = $th->getMessage();
            return view('panel.users.index', [
                'message' => $msg,
                'alert' => 'alert alert-danger',
            ]);
        } 
    }
}
