<?php

namespace App\Http\Controllers;

use App\Models\ModelTable;
use App\Models\User;
use App\Models\UserRole;
use Illuminate\Http\Request;

class DatabaseController extends Controller
{
    public function index()    
    {
        $user = User::whereNotNull('email_verified_at')->orWhere('id', '>', 3)->with('role')->first();
        
        echo $user->fullname;        
    }
}
