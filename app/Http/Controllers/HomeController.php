<?php

namespace App\Http\Controllers;

use App\Exceptions\MyCustomException;
use Illuminate\Http\Request;

class HomeController extends Controller
{    

    public function showHome(Request $request)
    {               
        try {
            return view('home', [
               'session' => $request->session()->get('user'), 
            ]);
            
        } catch (MyCustomException $e) {
            $msg =  $e->getMessage() . " en la línea " . $e->getLine() . " del archivo " . $e->getFile();
            return view('home', [
                'message' => $msg,
                'alert' => 'alert alert-danger',
            ]);
        } catch (\Throwable $th) {
            $msg = $th->getMessage();
            return view('home', [
                'message' => $msg,
                'alert' => 'alert alert-danger',
            ]);
        }                 
    }
}
